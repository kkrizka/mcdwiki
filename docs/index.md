# Muon Collider Detector and Physics Wiki

This wiki has moved to [https://mcd-wiki.web.cern.ch/](https://mcd-wiki.web.cern.ch/).

The archive of this page is available at [https://gitlab.cern.ch/kkrizka/mcdwiki/](https://mcd-wiki.web.cern.ch/).
